<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menampilkan Data Peminjam</title>
</head>
<body>
    <?php 
        include 'config.php';
        $db = new database();
    ?>

    <table border="1">
        <tr>
            <th>No</th>
            <th>Kode Peminjam</th>
            <th>Nama</th>
            <th>Jenis Kelamin</th>
            <th>Tanggal Lahir</th>
            <th>Alamat</th>
            <th>Pekerjaan</th>
            <th>Edit</th>
            <th>Hapus</th>
        </tr>

        <?php 
            $no = 1;
            foreach($db->tampil_data() as $x) {
        ?>
        <tr>
            <td><?= $no++; ?></td>
            <td><?= $x['kode_peminjam']; ?></td>
            <td><?= $x['nama_peminjam']; ?></td>
            <td><?= $x['jenis_kelamin']; ?></td>
            <td><?= $x['tanggal_lahir']; ?></td>
            <td><?= $x['alamat']; ?></td>
            <td><?= $x['pekerjaan']; ?></td>
            <td><a href="edit_data_peminjam.php?id=<?php echo $x['kode_peminjam'];?>">Edit</a></td>
            <td><a href="hapus_data_peminjam.php?id=<?php echo $x['kode_peminjam'];?>">Hapus</a></td>
        </tr>
        <?php 
            }
        ?>
    </table>
</body>
</html>